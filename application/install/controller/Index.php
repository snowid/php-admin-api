<?php
namespace app\install\controller;
use app\admin\model\SystemSafe;
use app\common\myconfig\Config as myConfig;
use think\Controller;
use think\Db;
use think\facade\Config;
use think\facade\Request;
use yichenthink\utils\ReturnMsg;

/**
 *
 */
class Index extends Base {
	public $root = '';
	public $module = [];
	protected function initialize() {
		$this->root = Request::root(true);
		$this->module = myConfig::$module;
	}
	public function index($value = '') {

		// 不带任何参数 自动定位当前操作的模板文件
		$this->assign('data', ['root' => $this->root, 'model' => $this->module]);
		return $this->fetch();
	}
	// 后台系统管理板块
	public function system($model = '') {
		// $config = new Config;
		$database = Config::get()['database'];
		$db_config1 = isset($database['db_' . $model]) ? $database['db_' . $model] : $database;
		//验证表是否存在
		//这是要查询的表的原始名称【没有加配置前缀】，$data['table_name']。
		$tableName = $db_config1['prefix'] . 'system_safe';

		$isTable = true;
		try {
			$db = Db::connect($db_config1);
			$db->query('SHOW TABLES LIKE ' . "'" . $tableName . "'");
		} catch (\Exception $e) {
			$isTable = false;
		}
		// ReturnMsg::returnMsg(200, '成功写入条数据' . $isTable, $db);
		$admin = ['username' => 'admin', 'password' => 'admin'];

		$model = isset($this->module[$model]) ? $this->module[$model] : [
			'title' => '未知',
			'model' => $model,
		];
		$model['root'] = $this->root;
		$data = ['user' => $admin, 'model' => $model, 'db' => $db_config1];
		// 不带任何参数 自动定位当前操作的模板文件
		$this->assign('data', $data);
		return $this->fetch();
	}
	// 内容新闻板块
	public function item($model = 'news') {

		$database = Config::get()['database'];
		$db_config = isset($database['db_' . $model]) ? $database['db_' . $model] : $database;
		//验证表是否存在
		//这是要查询的表的原始名称【没有加配置前缀】，$data['table_name']。
		$tableName = $db_config['prefix'] . 'system_safe';

		$isTable = true;
		try {
			$db = Db::connect($db_config);
			$db->query('SHOW TABLES LIKE ' . "'" . $tableName . "'");
		} catch (\Exception $e) {
			$isTable = false;
		}

		$model = isset($this->module[$model]) ? $this->module[$model] : [
			'title' => '未知',
			'model' => $model,
		];
		$model['root'] = $this->root;
		$data = ['model' => $model, 'db' => $db_config];
		// 不带任何参数 自动定位当前操作的模板文件
		$this->assign('data', $data);
		return $this->fetch();
	}
}
?>