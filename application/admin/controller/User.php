<?php
namespace app\admin\controller;
use app\admin\model\User as UserModel;
use yichenthink\utils\MakeId;
use yichenthink\utils\ReturnMsg;

class User extends Base {
	public function index() {
		$M = new User;
		$data = 'userindex';
		ReturnMsg::returnMsg($code = 200, $message = '成功', $data);
	}
// 查询列表
	public function list($p = 0, $last_time = null, $start_time = null, $area = null, $username = null, $trashed = null) {
		$num = 100;
		// 查找页数
		$startNum = $num * $p;
		// 按注册时间查找
		$last_time = isset($last_time) ? $last_time : time();
		$start_time = isset($start_time) ? $start_time : ($last_time - 3650 * 86400);
		$code = 400;
		$message = '';
		$map = [];
		$map[] = ['create_time', 'between', [$start_time, $last_time]];
		// 按地区查找
		if ($area) {
			$map[] = ['area_id', 'like', $area . '%'];
		}
		// 按用户名查找
		if ($username) {
			$map[] = ['username', 'like', $username . '%'];
			# code...
		}

		// 查询被删除的
		if ($trashed) {
			$data = UserModel::onlyTrashed()
				->where($map)
				->with(['area'])
				->visible(['id', 'nickname', 'username', 'create_time', 'update_time', 'state', 'area', 'city'])
				->limit($startNum, $num)
				->select();

		} else {
			// 查询未删除的
			$data = UserModel::where($map)
				->visible(['id', 'nickname', 'username', 'create_time', 'update_time', 'state', 'area', 'city'])
				->with(['area'])
				->limit($startNum, $num)
				->select();
		}
		if ($data) {
			$code = 200;
			$message = '成功';
		} else {
			$message = '没找到数据';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}

	// 修改
	public function update($id, $form = []) {

		//设置允许修改的字段
		$upfield = ['password' => 'password', 'state' => 'state'];
		$message = '';
		// 判断是不是超级管理员 防止无意删除超级账号
		if ($id != $this->safe['user_id'] || $this->isAdmin) {
			$user = UserModel::where('id', $id)->find();
			if (null != $user) {
				foreach ($upfield as $key => $value) {
					# code...
					if (isset($form[$key]) && !empty($form[$key])) {
						# code...
						$user->$value = $form[$key];
					}
				}
				$R = $user->save();
				if (false !== $R) {

					$code = 200;
				} else {
					$message = '操作失败';
					$code = 400;
				}
			} else {
				$message = '用户不存在';
				$code = 400;
			}
		} else {
			$message = "无权的操作";
		}
		ReturnMsg::returnMsg($code, $message);
	}
	// 删除
	public function delete($id = 0, $isTrue = false) {
		$msg = '';
		$code = 400;
		// 判断是不是超级管理员 防止无意删除超级账号
		if ($id != $this->safe['user_id'] || $this->isAdmin) {
			$user = UserModel::get($id);
			$user->delete($isTrue);
			if (0 !== $user) {
				$code = 200;
			}
		} else {
			$msg = "无权的超作";
		}

		ReturnMsg::returnMsg($code, $msg);
	}
	// 删除全部
	public function deleteAll($list = [], $isTrue = false) {
		// $mess = UserModel::destroy($list); //全部删除
		$num = 0;
		$user = [];

		foreach ($list as $key => $value) {
			// 判断是不是超级管理员 防止无意删除超级账号
			if ($value != $this->safe['user_id'] || $this->isAdmin) {
				$user = UserModel::get($value);
				if ($user) {
					$num++;
					# code...
					$user->delete($isTrue); //逐个删除
					unset($list[$key]);
				}
			}
		}
		if (count($list) > 0) {
			ReturnMsg::returnMsg($code = 206, '成功删除了' . $num . '条数据,但有' . count($list) . '条数据删除失败', $list);
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code = 200, '成功删除了' . $num . '条数据', ['num' => $num]);
	}
	//批量恢复
	public function restoreAll($list = []) {
		$num = 0;
		$user = [];
		foreach ($list as $key => $value) {
			$user = UserModel::onlyTrashed()->find($value);
			# code...
			if ($user) {
				$num++;
				# code...
				$user->restore();
				unset($list[$key]);
			}
		}
		if (count($list) > 0) {
			ReturnMsg::returnMsg($code = 206, '成功恢复了' . $num . '条数据,但有' . count($list) . '条数据恢复失败', $list);
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code = 200, '成功恢复了' . $num . '条数据');
	}
	//恢复一条
	public function restore($id = 0) {
		$num = 0;
		$user = UserModel::onlyTrashed()->find($id);
		# code...
		if ($user) {
			$num++;
			# code...
			$user->restore();
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code = 200, '成功恢复了' . $num . '条数据');
	}
	// 增加数据
	public function add($username, $password) {
		$message = '';
		$code = 400;
		// utils('ReturnMsg')::returnMsg($code, $message, $username . $password);
		$R = UserModel::where('username', $username)->find();

		if (Null == $R) {
			$user = new UserModel;
			$user->username = $username;
			$user->password = $password;
			$user->id = MakeId::User();
			$user->nickname = $username;
			$R = $user->save();
			if (false !== $R) {
				$code = 200;
				$data = UserModel::where('id', $user->id)
					->visible(['id', 'nickname', 'username', 'create_time', 'update_time', 'state'])
					->find();
				utils('ReturnMsg')::returnMsg($code, $message, $data);
			}
		} else {
			$message = '用户已存在';
		}
		utils('ReturnMsg')::returnMsg($code, $message, $R);
	}
}
