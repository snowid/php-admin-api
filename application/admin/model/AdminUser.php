<?php
namespace app\admin\model;

class AdminUser extends \app\common\model\AdminUser {

	protected function setIpAttr() {
		return request()->ip();
	}
	// 关联权限中间模型
	public function User() {
		return $this->hasOne('user', 'id', 'user_id')->bind([
			'username' => 'username',
			'nickname' => 'nickname',
			'area_name' => 'area_name',
		]);
	}
	// 关联主表模型
	public function admin() {
		return $this->hasOne('admin', 'id', 'admin_id')->bind([
			'group_id' => 'group_id',
			'admin_name' => 'admin_name',
			// 'delete_time' => 'deletetime',
		]);
	}
	// 关联职位模型
	public function adminRank() {
		return $this->hasOne('admin_rank', 'id', 'rank_id')->bind([
			'rank_name' => 'rank_name',
		]);
	}

	// 关联职位模型
	public function AdminRankFunc() {
		return $this->hasMany('admin_rank_func', 'rank_id', 'rank_id');
		// return $this->hasOne('admin_func', 'id', 'admin_rank_func.func_id');
	}

}