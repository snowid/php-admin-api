<?php
namespace app\system\controller;
use app\system\model\AdminGroupFunc as Mod;
use yichenthink\utils\ReturnMsg;

class AdminGroupFunc extends AdminBase {

	// 根据职务权限id 批量增加或删除一些功能 已废弃 采用单独的删除/添加方法
	public function update($rank_id, $func = []) {
		$message = '';
		$code = 400;
		$map = [];

		$Mod = new Mod;
		$old = $Mod->where('rank_id', $rank_id)
			->visible(['func_id'])
			->select();
		$oldFunc = [];
		for ($i = 0; $i < count($old); $i++) {
			$oldFunc[] = $old[$i]['func_id'];
		}
		// 匹配差异新数据
		$add = array_diff($func, $oldFunc);
		// 匹配差异删除的数据
		$del = array_diff($oldFunc, $func);
		// 批量添加
		$a = $Mod->add($rank_id, $add);
		// 批量删除
		$d = $Mod->del($rank_id, $del);
		// 返回结果

		$ab = [$a, $d];
		// ReturnMsg::returnMsg($code, $message = $rank_id, $ab);
		if (null != $a) {
			$message .= '新增' . count($a) . '条数据';
			$code = 200;
		}
		if (null != $d) {
			$message .= ',删除' . $d . '条数据';
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $ab);
	}
	// 根据职务权限id，增加一个或多个功能 $func=func_id
	public function add($group_id, $func_id) {

		$message = '';
		$code = 400;

		$Mod = new Mod;
		$Mod->group_id = $group_id;
		$Mod->func_id = $func_id;
		$a = $Mod->save();
		// $a = $Mod->save($map);
		if (null != $a) {
			$message .= '新增1条数据';
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $Mod);
	}
	// 根据职务权限id删除一个功能
	public function delete($group_id = 0, $func_id) {

		$code = 400;
		$map = ['group_id' => $group_id, 'func_id' => $func_id];

		$old = Mod::where($map)
			->visible(['id'])
			->find();
		if ($old) {
			$code = 200;
			$old->delete();
		}

		ReturnMsg::returnMsg($code, '', $old);
	}

}
