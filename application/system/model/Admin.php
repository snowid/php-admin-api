<?php
namespace app\system\model;

class Admin extends \app\common\model\Admin {

	// 关联权限中间模型
	public function User() {
		return $this->hasOne('user', 'id', 'user_id')->bind([
			'username' => 'username',
			'nickname' => 'nickname',
			'area_name' => 'area_name',
		]);
	}
	// 关联分组模型
	public function adminGroup() {
		return $this->hasOne('adminGroup', 'id', 'group_id')->bind([
			'group_name' => 'group_name',
		]);
	}
	// 关联分组模型
	public function adminGroupFunc() {
		return $this->hasMany('adminGroupFunc', 'group_id', 'group_id');
	}
	// 关联认证权限模型
	public function adminUser() {
		return $this->hasMany('admin_user', 'admin_id', 'id');

	}
	public function adminFunc() {
		return $this->hasMany('adminFunc', 'group_id', 'group_id');
	}
	public function adminRank() {
		return $this->hasMany('adminRunk', 'admin_id', 'admin_id');
	}
}