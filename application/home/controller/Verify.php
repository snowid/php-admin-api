<?php
namespace app\home\controller;
use think\facade\Session;
use yichenthink\captcha\Captcha;
use yichenthink\utils\ReturnMsg;

class Verify extends Base {

	public $prefix = 'verify';

	public function initialize() {
		parent::initialize();
		Session::init([
			'prefix' => 'home',
			'type' => '',
			'auto_start' => true,
			'id' => $this->prefix . $this->token['uid'],
		]);
	}
	public function index() {
		$captcha = new Captcha();
		return $captcha->entry();
	}
	// outType=base64/image
	public function captcha($id = 'captcha', $outType = 'image') {
		$captcha = new Captcha();
		$captcha->length = 4;
		$captcha->useNoise = false;
		$captcha->fontSize = 30;
		$captcha->outType = $outType;
		return $captcha->entry($id);

	}
	public function verifyCaptcha($captcha = '23', $id = 'captcha') {
		if (!captcha_check($captcha, $id)) {
			ReturnMsg::returnMsg(400, '验证码有误' . $captcha, $this->token);
			// 验证失败
		}
		return true;
	}

}
