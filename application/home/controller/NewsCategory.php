<?php
namespace app\home\controller;
use app\home\model\NewsCategory as Mod;
use think\facade\Request;
use yichenthink\utils\FileBase64;
use yichenthink\utils\ReturnMsg;

class NewsCategory extends Base {
	// 查询系统分类列表

	public function list() {
		$message = '没有数据';
		$code = 400;
		// 查询未删除的
		$data = Mod::where(['state' => '1'])->select();
		if ($data) {
			$code = 200;
			$message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $data->visible(['id', 'ranking', 'name', 'image', 'url', 'pid']));
	}

	//恢复一条
	public function restore($id = 0) {

		$map = [];
		$map[] = ['id', '=', $id];
		$code = 400;
		$Mod = Mod::onlyTrashed()
			->where($map)
			->find();
		# code...
		if ($Mod) {
			$code = 200;
			$Mod->restore();
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code, '', $Mod);
	}

}
