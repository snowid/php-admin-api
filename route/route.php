<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// Route::get('think', function () {
//     return 'hello,ThinkPHP5!';
// });

// Route::get('hello/:name', 'index/hello');

// return [

// ];
Route::group('admin', function () {
	Route::get('', 'list');
	Route::put('', 'update');
	Route::put('', 'restore');
	Route::delete('delete/:id', 'delete');
	Route::post('', 'add');
})->prefix('admin/')->pattern(['id' => '\d+'])->allowCrossDomain();

Route::group('system_safe_grade', function () {
	Route::get('', 'list');
})->prefix('system_safe_grade/')->allowCrossDomain();

Route::group('system_safe', function () {
	Route::get('', 'list');
	Route::put('', 'update');
})->prefix('system_safe/')->pattern(['id' => '\d+'])->allowCrossDomain();

Route::group('system_safe_url', function () {
	Route::get('', 'list');
	Route::put('', 'update');
	Route::delete('', 'delete');
	Route::put('', 'restore');
	Route::post('', 'add');
})->prefix('system_safe/')->pattern(['p' => '\d+', 'id' => '\d+'])->allowCrossDomain();

Route::group('admin_user', function () {
	Route::get('', 'list');
	Route::put('', 'update');
	Route::delete('', 'delete');
	Route::put('', 'restore');
	Route::post('', 'add');
	Route::put('', 'MyUpdate');
})->prefix('admin_user/')->pattern(['p' => '\d+', 'id' => '\d+'])->allowCrossDomain();

Route::group('admin_func', function () {
	Route::get('', 'list');
	Route::put('', 'update');
	Route::delete('', 'delete');
	Route::put('', 'restore');
	Route::post('', 'add');
	Route::get('', 'detailedList');
})->prefix('admin_func/')->pattern(['p' => '\d+', 'id' => '\d+'])->allowCrossDomain();

Route::group('admin_rank', function () {
	Route::get('', 'list');
	Route::get('', 'MyRankFunc');
	Route::put('', 'update');
	Route::delete('', 'delete');
	Route::put('', 'restore');
	Route::post('', 'add');
})->prefix('admin_rank/')->pattern(['id' => '\d+'])->allowCrossDomain();

Route::group('admin_rank_func', function () {
	Route::put('', 'update');
	Route::delete('', 'delete');
	Route::post('', 'add');
})->prefix('admin_rank_func/')->pattern(['id' => '\d+'])->allowCrossDomain();

Route::group('admin_group_func', function () {
	Route::delete('', 'delete');
	Route::post('', 'add');
})->prefix('admin_group_func/')->pattern(['id' => '\d+'])->allowCrossDomain();

Route::group('admin_group', function () {
	Route::get('', 'list');
	Route::get('', 'funcList');
	Route::put('', 'update');
	Route::delete('', 'delete');
	Route::put('', 'restore');
	Route::post('', 'add');
})->prefix('admin_group/')->pattern(['id' => '\d+'])->allowCrossDomain();

Route::group('user', function () {
	Route::get('', 'list');
	Route::put('', 'update');
	Route::delete('', 'delete');
	Route::delete('', 'deleteAll');
	Route::put('', 'restore');
	Route::put('', 'restoreAll');
	Route::post('', 'add');
})->prefix('user/')->pattern(['id' => '\d+', 'p' => '\d+']);

Route::group('login', function () {
	Route::get('', 'login');
	Route::put('', 'resetLogin');
	Route::post('', 'register');
})->prefix('login/');

Route::group('verify', function () {
	Route::get('', 'captcha');
})->prefix('verify/');

// Route::get('install', 'install/index');
return [

];
