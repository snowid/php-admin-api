﻿# Host: 127.0.0.1  (Version 8.0.12)
# Date: 2019-04-20 17:19:52
# Generator: MySQL-Front 6.1  (Build 1.21)

#
# Structure for table "news"
#

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `state` int(11) DEFAULT '1' COMMENT '状态0=屏蔽 默认是1可见',
  `user_id` bigint(16) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `comment` int(11) unsigned DEFAULT '0' COMMENT '评论数量',
  `zan` int(11) unsigned DEFAULT '0' COMMENT '赞数量',
  `collect` int(11) unsigned DEFAULT '0' COMMENT '收藏数量',
  `report` int(11) unsigned DEFAULT '0' COMMENT '举报次数',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned DEFAULT '0' COMMENT '更新时间',
  `delete_time` int(11) unsigned DEFAULT '0' COMMENT '删除时间',
  `category_id` tinyint(3) unsigned DEFAULT '0' COMMENT '分类id',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `update_time` (`update_time`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='微博';

#
# Data for table "news"
#

INSERT INTO `news` VALUES (196,1,10101555674757204,0,0,0,0,1555910799,1555910799,0,55),(197,1,10101555674757204,0,0,0,0,1555911041,1555911041,0,55),(198,1,10101555674757204,0,0,0,0,1555911090,1555911090,0,55);

#
# Structure for table "news_category"
#

DROP TABLE IF EXISTS `news_category`;
CREATE TABLE `news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '未命名',
  `pid` int(3) unsigned DEFAULT '0' COMMENT '父id',
  `ranking` tinyint(3) unsigned DEFAULT '0' COMMENT '排名排序',
  `image` varchar(255) DEFAULT '0' COMMENT '图片',
  `url` varchar(255) DEFAULT '0' COMMENT '链接地址',
  `state` tinyint(3) unsigned DEFAULT '1' COMMENT '1=显示，0=隐藏',
  `user_id` bigint(20) unsigned DEFAULT '0' COMMENT '操作员id',
  `genealogy` varchar(255) DEFAULT NULL COMMENT '族谱id,记录上级全系id,分割',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT '0',
  `delete_time` int(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

#
# Data for table "news_category"
#

INSERT INTO `news_category` VALUES (50,'第一个菜单-1',55,10,'news/19/04/22/1555912065121444.jpeg','0',1,101552115164859,'[55]',1554467677,1555913015,0),(55,'第一个菜单',0,10,'news/19/04/22/1555912059295325.jpeg','0',1,101552115164859,'[0]',1554463441,1555912059,0),(71,'顶级菜单',0,10,'news/19/04/22/1555912966583200.jpeg','0',1,10101555674757204,'[0]',1555912966,1555912966,0);

#
# Structure for table "news_collect"
#

DROP TABLE IF EXISTS `news_collect`;
CREATE TABLE `news_collect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `t_id` int(11) unsigned DEFAULT NULL COMMENT '帖子id',
  `user_id` bigint(16) unsigned DEFAULT NULL COMMENT '用户id',
  `createtime` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  `tag` int(1) unsigned DEFAULT '1' COMMENT '1=已收藏，2=取消收藏',
  `note` char(50) DEFAULT '备注' COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `uid` (`user_id`),
  KEY `createtime` (`createtime`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='收藏';

#
# Data for table "news_collect"
#


#
# Structure for table "news_comment"
#

DROP TABLE IF EXISTS `news_comment`;
CREATE TABLE `news_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '1=主题评论，2等于回复',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '评论内容',
  `user_id` bigint(16) unsigned DEFAULT NULL COMMENT '发言用户id',
  `t_id` int(11) DEFAULT NULL COMMENT '主题id',
  `r_id` int(11) DEFAULT NULL COMMENT '被回复的评论id',
  `createtime` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  `reply` int(11) unsigned DEFAULT '0' COMMENT '回复总数',
  `zan` int(11) unsigned DEFAULT '0' COMMENT '点赞次数',
  `refresh` int(11) unsigned DEFAULT '0' COMMENT '刷新时间',
  `report` int(11) DEFAULT '0' COMMENT '举报次数',
  `collect` int(11) unsigned DEFAULT '0' COMMENT '收藏次数',
  PRIMARY KEY (`id`),
  KEY `uid` (`user_id`),
  KEY `rid` (`r_id`,`type`,`refresh`),
  KEY `tid` (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='评论';

#
# Data for table "news_comment"
#


#
# Structure for table "news_comment_zan"
#

DROP TABLE IF EXISTS `news_comment_zan`;
CREATE TABLE `news_comment_zan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `t_id` int(11) unsigned DEFAULT NULL COMMENT '评论id',
  `user_id` bigint(16) unsigned DEFAULT NULL COMMENT '用户id',
  `refresh` int(11) unsigned DEFAULT NULL COMMENT '刷新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='评论点赞记录';

#
# Data for table "news_comment_zan"
#


#
# Structure for table "news_content"
#

DROP TABLE IF EXISTS `news_content`;
CREATE TABLE `news_content` (
  `t_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '主键id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '内容',
  `image` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '主图',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '无标题' COMMENT '标题',
  PRIMARY KEY (`t_id`),
  CONSTRAINT `news_content_ibfk_1` FOREIGN KEY (`t_id`) REFERENCES `news` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='微博';

#
# Data for table "news_content"
#

INSERT INTO `news_content` VALUES (196,'这是今天的第一个文章','news/19/04/22/1555910799859364.jpeg','发布一篇电影叫寒战'),(197,'这是一部有周星驰导演的科幻电影','news/19/04/22/1555911041245231.jpeg','电影美人鱼'),(198,'这是一部动作电影，拳王打斗很霸气','news/19/04/22/1555911090412299.jpeg','电影拳霸天下');

#
# Structure for table "news_image"
#

DROP TABLE IF EXISTS `news_image`;
CREATE TABLE `news_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id=图片名',
  `suffix` char(4) DEFAULT '' COMMENT '后缀名',
  `t_id` int(11) unsigned DEFAULT '0' COMMENT '所属主题帖子id',
  `user_id` bigint(16) unsigned DEFAULT '0' COMMENT '用户id',
  `createtime` int(11) unsigned DEFAULT '0' COMMENT '创建时间',
  `src` varchar(50) DEFAULT '0' COMMENT '图片地址',
  `size` int(11) unsigned DEFAULT '0' COMMENT '大小字节',
  `type` varchar(255) DEFAULT '0' COMMENT '资源类型',
  `content_type` varchar(255) DEFAULT '0' COMMENT '资源类型全称',
  `delete_time` int(11) unsigned DEFAULT '0' COMMENT '删除时间',
  `update_time` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='本类下图库';

#
# Data for table "news_image"
#

INSERT INTO `news_image` VALUES (241,'jpeg',196,10101555674757204,0,'news/19/04/22/1555910799859364.jpeg',1576127,'image','image/jpeg',0,1555910799),(242,'jpeg',196,10101555674757204,0,'news/19/04/22/1555910949678380.jpeg',1155454,'image','image/jpeg',0,1555910949),(243,'jpeg',197,10101555674757204,0,'news/19/04/22/1555911041245231.jpeg',670932,'image','image/jpeg',0,1555911041),(244,'jpeg',198,10101555674757204,0,'news/19/04/22/1555911090412299.jpeg',1613953,'image','image/jpeg',0,1555911090);

#
# Structure for table "news_relation"
#

DROP TABLE IF EXISTS `news_relation`;
CREATE TABLE `news_relation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `table` varchar(255) DEFAULT NULL COMMENT '关联的表名',
  `table_id` int(11) unsigned DEFAULT NULL COMMENT '关联表id',
  `t_id` varchar(255) DEFAULT NULL COMMENT '上级主表id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='关联外表';

#
# Data for table "news_relation"
#


#
# Structure for table "news_tag"
#

DROP TABLE IF EXISTS `news_tag`;
CREATE TABLE `news_tag` (
  `news_id` int(11) unsigned DEFAULT NULL COMMENT '关联文章表id',
  `tag_id` int(11) unsigned DEFAULT NULL COMMENT '关联标签表id',
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='标签表';

#
# Data for table "news_tag"
#


#
# Structure for table "news_zan"
#

DROP TABLE IF EXISTS `news_zan`;
CREATE TABLE `news_zan` (
  `tid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '被点赞的帖子id',
  `user_id` bigint(16) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `refresh` varchar(255) DEFAULT '' COMMENT '上次操作时间',
  PRIMARY KEY (`id`),
  KEY `tid` (`tid`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='主题点赞';

#
# Data for table "news_zan"
#